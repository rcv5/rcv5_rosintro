#!/usr/bin/bash

rosservice call /spawn 7.5 7.4 2.5 'turtle2'
rosservice call turtle1/set_pen 255 100 0 3 0 
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , 4.0 , 0.0] ' '[0.0 , 0.0 , 0.0] '
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[4.0 , 0.0 , 0.0]' '[0.0 , 0.0 , -3.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-1.0, 1.5, -1.5] ' '[0.0 , 0.0 , 0.0]' \

rosservice call turtle2/set_pen 100 100 0 3 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[-1.5,-1.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[5.6, 0.0, 1.0]' '[0.0, 0.0, 4.0]'
