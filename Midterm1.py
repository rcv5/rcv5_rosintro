#Reyna Vrbensky 
#load in all packages (from tutorials) for ROS interface

from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt
from std_msgs.msg import String


from moveit_commander.conversions import pose_to_list

#Create ROS node and Moveit Commander Link
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

#Create robot object and scene/enviroment 
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

#Create joint group for ur5 robot (not pandas)
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
#Command to display trajectory in RVis
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

#Move Robot to more ideal Starting Configuration to avoid singularity
tau = 2.0 * pi #declare angle of 360 degrees
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8
joint_goal[2] = 0
joint_goal[3] = -tau / 4
joint_goal[4] = 0
joint_goal[5] = tau / 6  

#This function intakes a 1x3 matrix that has the x,y, and z coordinates of the desired position for the end effector. The function moves the robot to the 
#position specified by this matrix
def translate(xyz):
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 1.0
    pose_goal.position.x = xyz[0]
    pose_goal.position.y = xyz[1]
    pose_goal.position.z = xyz[2]
    move_group.set_pose_target(pose_goal)
    # `go()` returns a boolean the planning and execution was successful or not.
    success = move_group.go(wait=True)
    # Calling `stop()` to make sure there is no residual movement and maintain accuracy
    move_group.stop()
    # It is always good to clear your targets after planning with poses.
    # Note: there is no equivalent function for clear_joint_value_targets().
    move_group.clear_pose_targets()

xyzpoints = [] #initialize an empty list to contain all of the coordinates 


#Letter R
xyzpoints.append([.4,.492,.535])
xyzpoints.append([.4,.592,.535])
xyzpoints.append([.4,.592,.485])
xyzpoints.append([.4,.492,.485])
xyzpoints.append([.4,.592,.33])

#Letter C
xyzpoints.append([.4,.492,.635])
xyzpoints.append([.4,.392,.535])
xyzpoints.append([.4,.392,.435])
xyzpoints.append([.4,.492,.435])

#Letter V
xyzpoints.append([.4,.492,.535])
xyzpoints.append([.639,.48,0.3])
xyzpoints.append([.636,.492,.535])



#Foreach iteration the list 'xyzpints' is updated then the translate function is used to move the robot to every desired point, as defined in input xyz
for x in xyzpoints:
    translate(x)
